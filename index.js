class Student {
	constructor(name,email,grades) {
		this.name = name;
		this.email = email;
		this.average = undefined;
		this.isPassed = undefined;
		this.isPassedWithHonors = undefined;

		if(grades.some(grade => typeof grade !== 'number')){
			this.grades = undefined
		} else{
			this.grades = grades;
		}
	}

	login(){
        console.log(`${this.email} has logged in`);
        return this;
    }

    logout(){
        console.log(`${this.email} has logged out`);
        return this;
    }

    listGrades(){
        this.grades.forEach(grade => {
            console.log(grade);
        })
        return this;
    }

    computeAve(){
        let sum = this.grades.reduce((accumulator,num) => accumulator += num)
        this.average = Math.round(sum/4);
        return this;
    }

    willPass() {
        this.isPassed = Math.round(this.computeAve().average) >= 85 ? true : false;
        return this;
    }

    willPassWithHonors() {
       this.isPassedWithHonors = this.willPass().isPassed && Math.round(this.computeAve().average) >= 90 ? true : this.willPass().isPassed ? false : undefined;
       return this;
    }
}

// Class Section will allow us to group our students as a section.
class Section{
    // Every instance of Section class will be instatiated with an empty array for our students.
    constructor(name){
        this.name = name;
        this.students = [];
        // add a counter or number of honor students:
        this.honorStudents = undefined;
        this.passedStudents = undefined;
        this.sectionAve = undefined;
    }
    // addStudents method will allow us to add instances of the student class as items for our students property.
    addStudent(name,email,grades){
        // A students instance/object will be instantiated with the name, email, grades and pushed into our students property.
        this.students.push(new Student(name,email,grades));
        return this;
    }

    // countHonorStudents will loop over each Student instance in our students array and count the number of students who will pass with honors.
    countHonorStudents(){
        // accumulate the number of honor students
        let count = 0;
            
        // loop over each student instance in the students array
        this.students.forEach(student => {

            // log the students instance currently being iterated.
            // console.log(student);

            // log if each student student's isPassedWithHonors property:
            // console.log(student.willPassWithHonors().isPassedWithHonors);
            
            // invoke will pass with honors so we can determine and add whether the student passed with honors in its property
            student.willPassWithHonors();

            // isPassedWithHonors property of the student should be populated.
            // console.log(student);

            // check if students isPassedWithHonor
            // add 1 to a temporary variable to hold the number of honorStudents.
            if(student.isPassedWithHonors){
                count++
            };
        })

        // update the honorStudent property with the update value of count:
        this.honorStudents = count;
        return this;
    }

    computeSectionAve() {
        let leng = 0;
        let sum = 0;
        this.students.forEach(student => {
            sum += student.average;
            leng++; 
        })
        this.sectionAve = Math.round(sum/leng);
        return this;
    }
    getNumberOfStudents() {
        let studentCount = 0;
        this.students.forEach(student => {
            studentCount++
        })
        console.log(`Number of students in ${this.name}:`,studentCount);
    }

    countPassedStudents() {
        let passerCount = 0;
        this.students.forEach(student => {
            if(student.willPass().isPassed){
                passerCount++
            }
        })
        this.passedStudents = passerCount;
        return this;

    }

}

let section1A = new Section("Section1A");
console.log(section1A);

section1A.addStudent("Joy","joy@gmail.com",[89,91,92,88]);
section1A.addStudent("Jeff","jeff@gmail.com",[81,80,82,78]);
section1A.addStudent("John","john@gmail.com",[81,90,92,96]);
section1A.addStudent("Jack","jack@gmail.com",[95,92,92,93]);
console.log(section1A);

// Check the details of John from section1A
// console.log(section1A.students[2]);

// check the average of John from section1A
// console.log("John's average:",section1A.students[2].computeAve().average);

// console.log("Jeff's average:",section1A.students[1].computeAve().average);
// console.log("Did Jeff pass?",section1A.students[1].willPass().isPassed);

// display the number of honor students in our section.
// section1A.countHonorStudents();
console.log(section1A);
// console.log(section1A.countHonorStudents().honorStudents);

  ///////////////////////////
 //   Activity Solution   //
///////////////////////////
section1A.addStudent("Alex","alex@gmail.com",[84,85,85,86]);

section1A.getNumberOfStudents();
section1A.countPassedStudents();
console.log("Number of student that passed in section1A:",section1A.passedStudents);
section1A.computeSectionAve();
console.log("Section1A average:",section1A.sectionAve);